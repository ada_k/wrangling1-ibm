#!/usr/bin/env python
# coding: utf-8

# In[7]:


#importing product dataset
import pandas as pd


product_data = pd.read_csv("Product Data Set - Student 2 of 3.csv", sep = '|') 
product_data.head()
#product_data


# In[9]:


#importing transaction dataset
import pandas as pd

transaction_data = pd.read_csv("Transaction Data Set - Student 3 of 3.csv", sep  = '|')
transaction_data.head()


# In[14]:


#importing customer dataset
import pandas as pd

customer_data = pd.read_csv("Customer Data Set - Student 1 of 3.csv")
customer_data.head()


# In[20]:


#data exploration
product_data.shape


# In[21]:


transaction_data.shape


# In[22]:


customer_data.shape


# In[23]:


#types and shit
type(product_data)


# In[24]:


type(transaction_data)


# In[25]:


type(customer_data)


# In[28]:


type(customer_data.AGE)


# In[61]:


customer_data.dtypes


# In[62]:


transaction_data.dtypes


# In[64]:


customer_data['INCOME']=customer_data['INCOME'].map(lambda x : x.replace('$',''))
customer_data['INCOME']= customer_data['INCOME'].map(lambda x : int(x.replace(',', '')))
customer_data.head()


# In[67]:


customer_data["INCOME"].describe()


# In[68]:


customer_data["MARITAL STATUS"].describe()


# In[69]:


customer_data["MARITAL STATUS"].unique()


# In[80]:


#converting 'enrollment date' to a datetime object
from datetime import datetime
customer_data['ENROLLMENT DATE']=customer_data['ENROLLMENT DATE'][customer_data['ENROLLMENT DATE'].notnull()].map(lambda x :datetime.strptime(x, '%d-%m-%Y') )


# In[81]:


customer_data.dtypes


# In[85]:


print(customer_data.isnull().values.any())
print(transaction_data.isnull().values.any())
print(product_data.isnull().values.any())


# In[89]:


print(customer_data.columns[customer_data.isna().any()].tolist())
print(customer_data.columns[customer_data.isnull().any()].tolist())


# In[97]:


#visualisation/graphs
import matplotlib.pyplot as plt

customer_data["MARITAL STATUS"].value_counts().plot(kind='bar')
plt.xlabel('marital status')
plt.ylabel('frequency')
plt.show()


# In[99]:


customer_data["LOYALTY GROUP"].value_counts().plot(kind='bar')
plt.xlabel('loyalty group')
plt.ylabel('frequency')
plt.show()


# In[100]:


customer_data["GENDER"].value_counts().plot(kind='bar')
plt.xlabel('gender')
plt.ylabel('frequency')
plt.show()


# In[102]:


customer_data["AGE"].hist(bins = 5)
plt.show()


# In[103]:


#plt.figure(figsize=(8,8))
plt.boxplot(customer_data.AGE,0,'rs',1)
#plt.grid(linestyle='-',linewidth=1)
plt.show()


# In[106]:


customer_data["AGE"].describe()


# In[107]:


trans_product = transaction_data.merge(product_data, how = 'inner', left_on ='PRODUCT NUM', right_on = 'PRODUCT CODE')
trans_product.head()


# In[109]:


print(product_data["PRODUCT CODE"].head())
print(transaction_data["PRODUCT NUM"].head())


# In[116]:


trans_product["UNIT LIST PRICE"] =trans_product["UNIT LIST PRICE"].map(lambda x: float(x.replace('$','')))


# In[118]:


trans_product.dtypes


# In[123]:


trans_product["TOTAL_PRICE"] = trans_product["QUANTITY PURCHASED"] *trans_product["UNIT LIST PRICE"] *(1 - trans_product["DISCOUNT TAKEN"])
trans_product.dtypes


# In[124]:


trans_product.head()

trans_product["PRODUCT CATEGORY"].unique()
# In[125]:


trans_product["PRODUCT CATEGORY"].unique()


# In[159]:


customer_category_sum = trans_product.groupby(['CUSTOMER NUM', 'PRODUCT CATEGORY']).agg({'TOTAL_PRICE':'sum'})
customer_category_sum.head()


# In[160]:


customer_category_sum.columns


# In[138]:


customer_category_sum = customer_category_sum.reset_index().head()


# In[161]:


Income_by_product = trans_product.groupby('PRODUCT CATEGORY').agg({'TOTAL_PRICE':'sum'}).sort_values('TOTAL_PRICE',ascending=False)
Revenue_by_product=Income_by_product.rename(columns={'TOTAL_PRICE':'Revenue Per Product'})
Income_by_product


# In[168]:


Revenue_by_product['Revenue Per Product'].plot(kind='pie',autopct='%1.1f%%',legend = True)


# In[169]:


customer_pivot = customer_category_sum.pivot(index= 'CUSTOMER NUM', columns='PRODUCT CATEGORY', values='TOTAL_PRICE')
customer_pivot


# In[182]:


from datetime import datetime
trans_product['TRANSACTION DATE']=trans_product['TRANSACTION DATE'].map(lambda x :datetime.strptime(x, '%m/%d/%Y') )


# In[179]:


print(trans_product.isnull().values.any())


# In[181]:


trans_product.dtypes


# In[197]:


dates_impact = trans_product.groupby('CUSTOMER NUM').agg({'TRANSACTION DATE':'max', 'TOTAL_PRICE':'sum'}). rename(columns={'TRANSACTION DATE':'RECENT DATE', 'TOTAL_PRICE':'TOTAL SPENT'})
dates_impact.head()


# In[189]:


customer_KPI = customer_pivot.merge(dates_impact, how='inner', left_index=True, right_index=True)
customer_KPI.head()


# In[192]:


customer_KPI = customer_KPI.fillna(0)
customer_KPI


# In[195]:


all_view = customer_data.merge(customer_KPI, how='inner', left_on='CUSTOMERID', right_index = True)
all_view


# In[201]:


#bivariate analysis
tab1 = pd.crosstab(all_view['GENDER'], all_view['LOYALTY GROUP'])
tab1


# In[202]:


tab1.plot(kind='bar', stacked=True)
plt.show()


# In[205]:


tab2 = pd.crosstab(all_view['EXPERIENCE SCORE'], all_view['LOYALTY GROUP'])
print(tab2)
tab2.plot(kind='bar', stacked=True)
plt.show()


# In[206]:


tab3 = pd.crosstab(all_view['MARITAL STATUS'], all_view['LOYALTY GROUP'])
print(tab3)
tab2.plot(kind='bar', stacked=True)
plt.show()


# In[207]:


tab4 = pd.crosstab(customer_data['MARITAL STATUS'], all_view['LOYALTY GROUP'])
print(tab3)
tab4.plot(kind='bar', stacked=True)
plt.show()


# In[208]:


tab5 = pd.crosstab(customer_data['GENDER'], all_view['LOYALTY GROUP'])
print(tab3)
tab5.plot(kind='bar', stacked=True)
plt.show()


# In[209]:


all_view.groupby("LOYALTY GROUP").agg({'AGE':'mean'})


# In[213]:


fig = plt.figure(1, figsize=(9, 6))
ax = fig.add_subplot(111)
plot1=all_view['AGE'][all_view['LOYALTY GROUP'] == "enrolled"]
plot2=all_view['AGE'][all_view['LOYALTY GROUP'] == "notenrolled"]
list1=[plot1,plot2]
ax.boxplot(list1,0,'rs',1)
ax.set_xticklabels(['Enrolled', 'Not Enrolled'])
plt.grid( linestyle='-', linewidth=1)
plt.show()


# In[215]:


all_view['TOTAL SPENT BINNED'] = pd.cut(all_view['TOTAL SPENT'],10) # 10 bins of age


# In[216]:


table=pd.crosstab(all_view['TOTAL SPENT BINNED'],all_view['LOYALTY GROUP'])
table.plot(kind='bar', stacked=True,figsize=(6,6))
plt.show()


# In[218]:


plt.scatter(all_view['AGE'],all_view['TOTAL SPENT'])
plt.xlabel("AGE")
plt.ylabel("Total Spent")
plt.show()


# In[219]:


from scipy.stats import pearsonr
pearsonr(all_view['AGE'],customer_all_view['TOTAL SPENT'])


# In[220]:


plt.scatter(all_view['INCOME'],all_view['TOTAL SPENT'])
plt.xlabel("Income")
plt.ylabel("Total Spent")
plt.show()


# In[221]:


pearsonr(all_view['INCOME'],all_view['TOTAL SPENT'])


# In[222]:


table = all_view.groupby(['EXPERIENCE SCORE']).agg({'TOTAL SPENT':'mean'}).reset_index()


# In[223]:


table['TOTAL SPENT'].plot(kind='bar')
plt.xlabel("Experience Score")
plt.ylabel("Average Total Spent per Score")
plt.xticks([0,1,2,3,4,5,6,7,8,9],[1,2,3,4,5,6,7,8,9,10])    
plt.show()


# In[ ]:




