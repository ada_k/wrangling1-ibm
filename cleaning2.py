#!/usr/bin/env python
# coding: utf-8

# In[146]:


import pandas as pd

london_air = pd.read_csv("LaqnData.csv")
london_air.head()


# In[147]:


london_air.describe()


# In[148]:


london_air['Provisional or Ratified'].unique()


# In[149]:


london_air['Species'].unique()


# In[150]:


london_air['Species'].describe()


# In[151]:


london_air['Site'].unique()


# In[152]:


london_air['ReadingDateTime'].unique()


# In[153]:


london_air['Units'].unique()


# In[154]:


london_air['Value'].unique()


# In[155]:


london_air.dtypes


# In[156]:


column_names = london_air.columns
for i in column_names:
  print('{} is unique: {}'.format(i, london_air[i].is_unique))


#  i = london_air.columns
# for column in i:
#     if column.is_unique:
#         print("this is unique {}".format(column))

# In[157]:


import numpy as np
london_air.index.values


# In[158]:


print(london_air.isnull().values.any())


# In[159]:


#print(london_air.columns[london_air.isna().any()].tolist())
print(london_air.columns[london_air.isnull().any()].tolist())


# In[160]:


london_air['Value'] = london_air['Value'].fillna(0)


# In[161]:


london_air.isnull().any()


# In[162]:


london_air


# In[163]:


london_air.dtypes


# In[107]:


from datetime import datetime

london_air['ReadingDateTime'] = london_air['ReadingDateTime'].map(lambda x: datetime.strptime(x, '%d/%m/%y %H:%M'))


# In[164]:


london_air.dtypes


# In[165]:


london_air['Species'].str.upper()


# In[166]:


london_air.duplicated().values.any()


# In[167]:


london_air.drop(['Site'], axis = 1)


# In[ ]:





# In[168]:


# import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')

plt.boxplot(london_air['Value'].values)
plt.grid( linestyle='-', linewidth=1)
plt.show()


# In[169]:


plt.scatter(london_air['Value'],london_air['Species'])
plt.xlabel("Value")
plt.ylabel("Species")
plt.show()


# In[170]:



london_air['Value'] = london_air['Value'].between(london_air['Value'].quantile(.25), london_air['Value'].quantile(.75))
london_air['Value'].values.any()


# In[173]:


plt.boxplot(london_air['Value'].values)
plt.grid( linestyle='-', linewidth=0.5)
plt.show()


# In[ ]:





# In[ ]:




